module.exports.Response = (message = "", data = [], success = true, error = false) => {

    success ? error = true : error = false
    error ? success = true : success = false

    return { message, data, success, error }
}