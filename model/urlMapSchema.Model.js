const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let urlMapSchema = new Schema({
    UrlMap: {
        type: String,
        required: true
    },
    UrlMapSlug: {
        type: String,
        required: true
    },
    Passcode: {
        type: String,
    },
    expireAt: {
        type: Date
    }
}, {
    timestamps: true
});
urlMapSchema.index({ UrlMap: 1, UrlMapSlug: 1, Passcode: 1, expireAt: 1 }, { unique: true })

const model = mongoose.model("urlmap", urlMapSchema);

module.exports = model;