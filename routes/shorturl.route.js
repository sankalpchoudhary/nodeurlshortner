const express = require('express');
const router = express.Router();
const urlShortCtrl = require('../modules/urlShortner/urlShortner.Controller.js')

/* GET users listing. */
router.get('/shortner', urlShortCtrl.urlShort);
router.get('/:shorturl', urlShortCtrl.redirectUrl);
module.exports = router;
