const redis = require('../connection/redis')
const { INVALID_ALLOWED_HITS, INVALID_SECOUNDS_WINDOW, RATE_LIMIT_EXCEED } = require('./Config')

function rateLimiter({ secoundsWindow, allowedHits }) {
    return async function (req, res, next) {
        const ip = req.ip

        if (secoundsWindow) {
            if (!(Number.isInteger(secoundsWindow))) {
                return INVALID_ALLOWED_HITS
            }
        }

        if (allowedHits) {
            if (!(Number.isInteger(allowedHits))) {
                return INVALID_SECOUNDS_WINDOW
            }
        }

        secoundsWindow = secoundsWindow ? secoundsWindow : 60
        allowedHits = allowedHits ? allowedHits : 10

        const requests = await redis.incr(ip)

        if (requests === 1) {
            await redis.expire(ip, secoundsWindow)
            secoundsWindow
        } else {
            await redis.ttl(ip)
        }

        if (requests > allowedHits) {
            return res.status(422).json({
                error: true,
                sucess: false,
                message: RATE_LIMIT_EXCEED,
                data: []
            })
        } else {
            next()
        }
    }
}
module.exports = rateLimiter