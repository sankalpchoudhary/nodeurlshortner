const fs = require('fs')
const { v4: uuidv4 } = require('uuid')
const Logger = require('../model/Log.Model')

function Intercenpter() {
    return async function (req, res, next) {
        const uuid = uuidv4()
        req.uuid = uuid
        res.uuid = uuid

        const resDotSendInterceptor = (res, json) => (content) => {
            res.contentBody = content;
            res.json = json;
            req.uuid = uuid
            res.json(content);
        };

        const reqData = {
            method: req.method,
            ip: req.ip,
            url: req.url,
            hostname: req.hostname,
            headers: req.headers,
            body: req.body,
            uuid: req.uuid
        }

        res.json = resDotSendInterceptor(res, res.json);

        res.on("finish", () => {
            const resData = {
                status: res.statusCode,
                data: res.contentBody,
                uuid: res.uuid
            }
            
            const LogData = JSON.stringify({ req: reqData, res: resData }) + ','
            Logger.create({ req: reqData, res: resData })
            fs.appendFileSync('logs/log.json', LogData)
        });
        next();
    }
}

module.exports = { Intercenpter }