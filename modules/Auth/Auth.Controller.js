const { Response } = require("../../Helper/Service");
const { INVALID_INPUT } = require("./Auth.Config");

module.exports = async (req, res) => {
    const v = new Validator(req.body, {
        email: 'required|email',
        password: 'required|string',
    });

    const matched = await v.check();

    if (!matched) {
        res.status = 422;
        return res.status(422).json(Response(INVALID_INPUT, v.errors));
    }
    
}