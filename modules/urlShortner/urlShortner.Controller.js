const urlShortnerService = require("./urlShortner.Service");
const { Validator } = require('node-input-validator');
var randomstring = require("randomstring");
const { SUCCESS_MSG, INVALID_TOKEN_ERROR, TECH_TEAM_MESSAGE } = require("./urlShortner.Config");


module.exports.urlShort = async (req, res) => {
    const v = new Validator(req.body, {
        url: 'required|url'
    });
    const matched = await v.check();

    if (!matched) {
        res.status = 422;
        return res.json({ errors: v.errors });
    }
    const { url } = req.body
    const Token = req.headers["x-api-token"]

    if (!(Token == "75dc60dc-c07b-430e-9bad-fb7440b5c206")) {
        return res.status(404).json({ data: [], error: true, success: false, message: INVALID_TOKEN_ERROR });
    }

    const ServicePrefix = process.env.SORT_SERVER_URL

    let UrlMapSlug = randomstring.generate(7)
    let CheckUrlMapSlug = await urlShortnerService.findOneShortUrlMap({ UrlMapSlug })
    let i = 0

    while (CheckUrlMapSlug) {
        UrlMapSlug = randomstring.generate(7)
        CheckUrlMapSlug = await urlShortnerService.findOneShortUrlMap({ UrlMapSlug })
        i++
        if (i > 1000) {
            return res.status(404).json({ data: {}, error: true, success: false, message: TECH_TEAM_MESSAGE });
        }
    }

    const AddShortUrlMapData = {
        UrlMapSlug,
        UrlMap: url
    }

    const AddUrlResponse = await urlShortnerService.addShortUrlMap(AddShortUrlMapData)
    const shortUrl = ServicePrefix + AddUrlResponse.UrlMapSlug
    return res.status(200).json({ data: { shortUrl, url: AddUrlResponse.UrlMap }, error: false, success: true, message: SUCCESS_MSG });
}

module.exports.redirectUrl = async (req, res) => {
    const v = new Validator(req.params, {
        shorturl: 'required|string'
    });

    const matched = await v.check();

    if (!matched) {
        res.status = 422;
        return res.json({ errors: v.errors });
    }
    const { shorturl } = req.params

    let CheckUrlMapSlug = await urlShortnerService.findOneShortUrlMap({ UrlMapSlug: shorturl })
    return res.redirect(CheckUrlMapSlug.UrlMap)
}