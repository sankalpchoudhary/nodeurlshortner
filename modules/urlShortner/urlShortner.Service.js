const urlMap = require('../../model/urlMapSchema.Model')

module.exports = class urlShortnerService {
    static async addShortUrlMap(data) {
        try {
            const ShortUrlMapAddResponse = await urlMap.create(data)
            return ShortUrlMapAddResponse
        } catch (error) {
            console.log('Error in add ShortUrlMap', error);
            throw new Error(error)
        }
    }

    static async findOneShortUrlMap(data) {
        try {
            const ShortUrlMapAddResponse = await urlMap.findOne(data)
            return ShortUrlMapAddResponse
        } catch (error) {
            console.log('Error in add ShortUrlMap', error);
            throw new Error(error)
        }
    }
}