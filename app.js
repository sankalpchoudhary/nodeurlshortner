require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');
const rateLimiter = require('./middleware/rateLimiter')

mongoose.set('strictQuery', true)

var shorturlRouter = require('./routes/shorturl.route');


/**
 *********** Middlewares *********
*/

/**
 *  Intercepter middleware Intercept Req Data and Response and Save it log file na DB
*/

const { Intercenpter } = require('./middleware/Intercenpter');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/s', Intercenpter(), rateLimiter({ secoundsWindow: 60, allowedHits: 2 }), shorturlRouter);

// Demonstrate the readyState and on event emitters
console.log(mongoose.connection.readyState); //logs 0
mongoose.connection.on('connecting', () => {
  console.log('connecting')
  console.log(mongoose.connection.readyState); //logs 2
});
mongoose.connection.on('connected', () => {
  console.log('connected');
  console.log(mongoose.connection.readyState); //logs 1
});
mongoose.connection.on('disconnecting', () => {
  console.log('disconnecting');
  console.log(mongoose.connection.readyState); // logs 3
});
mongoose.connection.on('disconnected', () => {
  console.log('disconnected');
  console.log(mongoose.connection.readyState); //logs 0
});

// Connect to a MongoDB server
mongoose.connect(process.env.MONGO_DB_URI, {
  useNewUrlParser: true // Boilerplate for Mongoose 5.x
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  return res.status(404).json({ data: {}, error: true, success: false, message: "Something Went Wrong" });
});

// error handler
app.use(function (err, req, res, next) {
  return res.status(404).json({ data: {}, error: true, success: false, message: "Something Went Wrong" });
});

module.exports = app;
